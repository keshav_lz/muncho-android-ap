package com.example.manishakeim.muncho_android_app.di.component;

import com.example.manishakeim.muncho_android_app.di.module.AppModule;
import com.example.manishakeim.muncho_android_app.di.module.NetModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ManishaKeim on 5/26/2018.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
}
