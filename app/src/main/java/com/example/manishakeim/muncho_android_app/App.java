package com.example.manishakeim.muncho_android_app;

import android.app.Application;

import com.example.manishakeim.muncho_android_app.di.component.NetComponent;
import com.example.manishakeim.muncho_android_app.di.module.AppModule;
import com.example.manishakeim.muncho_android_app.di.module.NetModule;

/**
 * Created by ManishaKeim on 5/26/2018.
 */

public class App extends Application {
    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mNetComponent = DaggerNetComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .netModule(new NetModule(""))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}
