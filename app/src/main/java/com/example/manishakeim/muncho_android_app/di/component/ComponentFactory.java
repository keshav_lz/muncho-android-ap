package com.example.manishakeim.muncho_android_app.di.component;

/**
 * Created by ManishaKeim on 5/26/2018.
 */

public class ComponentFactory {

    private static ComponentFactory componentFactory;

    private NetComponent netComponent;

    public static ComponentFactory getInstance() {
        if (componentFactory == null) {
            componentFactory = new ComponentFactory();
        }
        return componentFactory;
    }

    public NetComponent getNetComponent() {
        return netComponent;
    }
    public void removeNetComponent() {
        netComponent = null;
    }

}
