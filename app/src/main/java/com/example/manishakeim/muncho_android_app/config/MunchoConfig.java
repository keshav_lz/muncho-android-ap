package com.example.manishakeim.muncho_android_app.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ManishaKeim on 5/26/2018.
 */

public class MunchoConfig {
    private static final String KEY_API_URL = "";

    private final SharedPreferences mSharedPreferences;

    public MunchoConfig(Context context) {
        mSharedPreferences = context.getSharedPreferences("muncho_config", Context.MODE_PRIVATE);
    }
}
